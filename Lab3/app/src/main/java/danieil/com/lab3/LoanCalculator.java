package danieil.com.lab3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoanCalculator extends AppCompatActivity {

    private Button calcBtn;
    private Button clearBtn;
    private EditText amount;
    private EditText years;
    private EditText rate;
    private TextView monthly;
    private TextView totalP;
    private TextView totalI;
    private TextView bad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_calculator);

        //Buttons
        this.calcBtn = (Button) findViewById(R.id.calculateBtn);
        this.clearBtn = (Button) findViewById(R.id.clearBtn);

        //User Inputs
        this.amount = (EditText) findViewById(R.id.loanAmountTxt);
        this.years = (EditText) findViewById(R.id.yearsTxt);
        this.rate = (EditText) findViewById(R.id.interestTxt);

        this.monthly = (TextView) findViewById(R.id.monthlyPay);
        this.totalP = (TextView) findViewById(R.id.totalPay);
        this.totalI = (TextView) findViewById(R.id.totalInterst);
        this.bad = (TextView) findViewById(R.id.badInput);


    }

    public void calculate(View view){


        try {
            bad.setVisibility(View.GONE);
            double loanAmount = Double.parseDouble(amount.getText().toString());
            int numYears = Integer.parseInt(years.getText().toString());
            double interest = Double.parseDouble(rate.getText().toString());

            LoanCalculatorObject lc = new LoanCalculatorObject(loanAmount, numYears, interest);

            monthly.setText(Double.toString(lc.getMonthlyPayment()));
            totalP.setText(Double.toString(lc.getTotalCostOfLoan()));
            totalI.setText(Double.toString(lc.getTotalInterest()));
        }
        catch (Exception e){
            bad.setVisibility(View.VISIBLE);
        }

    }

    public void clear(View view){
        amount.setText("");
        years.setText("");
        rate.setText("");

        monthly.setText("");
        totalP.setText("");
        totalI.setText("");
    }
}

package course.labs.activitylab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

    // string for logcat documentation
    private final static String TAG = "Lab-ActivityOne";

    private int ctrCreate = 0;
    private int ctrStart = 0;
    private int ctrResume = 0;
    private int ctrPause = 0;
    private int ctrRestart = 0;
    private int ctrStop = 0;
    private int ctrDestroy = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_one);

        if (savedInstanceState != null) {
            ctrCreate = savedInstanceState.getInt("ctrCreate");
            ctrStart = savedInstanceState.getInt("ctrStart");
            ctrResume = savedInstanceState.getInt("ctrResume");
            ctrPause = savedInstanceState.getInt("ctrPause");
            ctrRestart = savedInstanceState.getInt("ctrRestart");
            ctrStop = savedInstanceState.getInt("ctrStop");
            ctrDestroy = savedInstanceState.getInt("ctrDestroy");
        }

        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        ctrCreate = sp.getInt("ctrCreate", 0);
        ctrStart = sp.getInt("ctrStart", 0);
        ctrResume = sp.getInt("ctrResume", 0);
        ctrPause = sp.getInt("ctrPause", 0);
        ctrRestart = sp.getInt("ctrRestart", 0);
        ctrStop = sp.getInt("ctrStop", 0);
        ctrDestroy = sp.getInt("ctrDestroy", 0);
        //Just setting it from the start
        ((TextView) findViewById(R.id.create)).setText(getResources().getString(R.string.onCreate) + " " + ctrCreate);
        ((TextView) findViewById(R.id.start)).setText(getResources().getString(R.string.onStart) + " " + ctrStart);
        ((TextView) findViewById(R.id.resume)).setText(getResources().getString(R.string.onResume) + " " + ctrResume);
        ((TextView) findViewById(R.id.pause)).setText(getResources().getString(R.string.onPause) + " " + ctrPause);
        ((TextView) findViewById(R.id.restart)).setText(getResources().getString(R.string.onRestart) + " " + ctrRestart);
        ((TextView) findViewById(R.id.stop)).setText(getResources().getString(R.string.onStop) + " " + ctrStop);
        ((TextView) findViewById(R.id.destroy)).setText(getResources().getString(R.string.onDestroy) + " " + ctrDestroy);


        //Log cat print out
        Log.i(TAG, "onCreate called");

        ctrCreate++;
        TextView create = (TextView) findViewById(R.id.create);
        //String str = create.getText().toString() + " " + ctrCreate;
        String temp = getResources().getString(R.string.onCreate);
        create.setText(temp + " " + ctrCreate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    // lifecycle callback overrides

    @Override
    public void onPause() {
        super.onPause();

        //Log cat print out
        Log.i(TAG, "onPause called");

        ctrPause++;
        String temp = getResources().getString(R.string.onPause);
        ((TextView) findViewById(R.id.pause)).setText(temp + " " + ctrPause);
    }

    @Override
    public void onResume() {
        super.onResume();

        //Log cat print out
        Log.i(TAG, "onStart called");

        ctrResume++;
        String temp = getResources().getString(R.string.onResume);
        ((TextView) findViewById(R.id.resume)).setText(temp + " " + ctrResume);
    }

    @Override
    public void onStart() {
        super.onStart();

        //Log cat print out
        Log.i(TAG, "onStart called");

        ctrStart++;
        String temp = getResources().getString(R.string.onStart);
        ((TextView) findViewById(R.id.start)).setText(temp + " " + ctrStart);
    }

    @Override
    public void onRestart() {
        super.onRestart();

        //Log cat print out
        Log.i(TAG, "onRestart called");

        ctrRestart++;
        String temp = getResources().getString(R.string.onRestart);
        ((TextView) findViewById(R.id.restart)).setText(temp + " " + ctrRestart);
    }

    @Override
    public void onStop() {
        super.onStop();

        //Log cat print out
        Log.i(TAG, "onStop called");

        ctrStop++;
        String temp = getResources().getString(R.string.onStop);
        ((TextView) findViewById(R.id.stop)).setText(temp + " " + ctrStop);

        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        sp.edit().putInt("ctrCreate", ctrCreate)
                .putInt("ctrStart", ctrStart)
                .putInt("ctrResume", ctrResume)
                .putInt("ctrPause", ctrPause)
                .putInt("ctrRestart", ctrRestart)
                .putInt("ctrStop", ctrStop)
                .putInt("ctrDestroy", ctrDestroy)
                .commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //Log cat print out
        Log.i(TAG, "onDestroy called");

        ctrDestroy++;
        String temp = getResources().getString(R.string.onDestroy);
        ((TextView) findViewById(R.id.destroy)).setText(temp + " " + ctrDestroy);

        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        sp.edit().putInt("ctrDestroy", ctrDestroy).commit();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("ctrCreate", ctrCreate);
        savedInstanceState.putInt("ctrStart", ctrStart);
        savedInstanceState.putInt("ctrResume", ctrResume);
        savedInstanceState.putInt("ctrPause", ctrPause);
        savedInstanceState.putInt("ctrRestart", ctrRestart);
        savedInstanceState.putInt("ctrStop", ctrStop);
        savedInstanceState.putInt("ctrDestroy", ctrDestroy);
    }

    public void launchActivityTwo(View view) {
        Intent activity2 = new Intent(getApplicationContext(), ActivityTwo.class);
        startActivity(activity2);
    }


}
